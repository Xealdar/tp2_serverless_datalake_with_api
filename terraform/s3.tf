# Créer un bucket aws_s3_bucket
resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-mollaretdenizot"
  acl    = "private"
  force_destroy = true
  tags = {
    Name        = "TP2Bucket"
    Environment = "Dev"
  }
}

# Créer un répertoire aws_s3_bucket_object avec job_offers/raw/
resource "aws_s3_bucket_object" "job_offers" {
  bucket = "s3-job-offer-bucket-mollaretdenizot"
  key    = "job_offers/"
  source = "/dev/null"
}

resource "aws_s3_bucket_object" "raw" {
  bucket = "s3-job-offer-bucket-mollaretdenizot"
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# Créer un event aws_s3_bucket_notification pour trigger la lambda

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.b.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.test_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  }
}


