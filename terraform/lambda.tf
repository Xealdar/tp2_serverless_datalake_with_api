# Créer le rôle pour la lambda avec aws_iam_role
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}



# Créer une lambda fonction aws_lambda_function
resource "aws_lambda_function" "test_lambda" {
  filename      = "empty_lambda_code.zip"
  function_name = "lambda_function_name"
  role          = "${aws_iam_role.iam_for_lambda.arn}"
  handler       = "lambda_main_app.lambda_handler"

  #depends_on    = [aws_iam_role_policy_attachment.lambda_logs, aws_cloudwatch_log_group.example]

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = "${filebase64sha256("lambda_function_payload.zip")}"

  runtime = "python3.7"

#   environment {
#     variables = {
#       foo = "bar"
#     }
#   }
}

resource "aws_cloudwatch_log_group" "example" {
  name              = "lambda_function_name"
  retention_in_days = 14
}

# Créer une aws_iam_policy pour le logging qui sera ajouté au rôle de la lambda
resource "aws_iam_policy" "lambda_logging" {
  name = "lambda_logging"
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
# Attacher la policy de logging au rôle de la lambda avec aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = "iam_for_lambda"
  policy_arn = "${aws_iam_policy.lambda_logging.arn}"
}

# Attacher la policy de AmazonS3FullAccess au rôle de la lambda avec aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess" {
  role = "iam_for_lambda"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


# Autoriser la lambda à être déclenchée par un event s3 avec aws_lambda_permission

resource "aws_lambda_permission" "allow_s3-job-offer-bucket-mollaretdenizot" {
  statement_id  = "AllowExecutionFromS3-job-offer-bucket-mollaretdenizot"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.test_lambda.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::s3-job-offer-bucket-mollaretdenizot"
}


# voir si faut une notif